

# Testing Framework

I have used BDD Test Automation framework with Java Serenity+Maven as suggested. This serenity BDD is a library which uses Cucumber and selenium for its development. Below are the advantages I see in using Serenity BDD

1) Easily maintainable automated acceptance criteria 
2) Living documentation of test results
3) Opensource tool with huge support Online 
4) Storing run time variables

# Project clean up	
1. Removed all gradle related files & dependancies, we can also use gradle as build tool, but removed those dependancies and files as it was obligated to use maven in assignment
1. Removed all unwanted maven dependancies (serenity - webdriver, screenplay, etc UI related)
1. Adding failsafe in maven-failsafe-plugin
1. Removed CarsAPI java class from Test step as I didn't see any pupose of this file

	

# Scenario coverage
As there was no clear functionalities defined for the given end point. So, considering the given test case for validating “title”, I have created scenarios with endpoint around validating the title

#### validating the title of the product
<details><summary>Click to expand</summary>
Scenario Outline: Verify  title content for each product retrieved

    When user request for a input with <Product> as valid product
    Then title for each <Product> is displayed as expected
    Examples:
      | Product |
      | tofu    |
      | water   |
      | apple   |
      | mango   |
</details>

##### Also a positive scenario of validating 200 response 


<details><summary>Click to expand</summary>
Scenario Outline: Verify  valid response is received when requested for a valid input

    When user request for a input with <Product> as valid product
    Then user receives a valid response
    Examples:
      | Product |
      | apple   |
      | mango   |
</details>

##### And negative scenario of validating 400 response
<details><summary>Click to expand</summary>
Scenario Outline: Verify error response is received when requested for a invalid product

    When user request for a input with <Product> as invalid product
    Then user receives a error response
    And error message is displayed
    Examples:
      | Product |
      | car     |
      | MANGO   |
</details>

# Installation, Execution 

## Pre-requisite to install
1. Maven is installed in the machine and configured properly
	
## Using Maven

    Open a command window and run:

        mvn clean verify
        
    To run cases with specific Tags
        
    mvn clean verify -Dcucumber.options="--tags @TagName" (EX: Smoke, Regression)

## Add new tests
	
	Any new tests are to be added under "src/test/resources/features" and steps definitions “src/test/java/starter/stepdefinitions/SearchStepDefinitions.java” accordingly


# Refactoring
Major refractoring is done in 2 areas
  
#### The way scenarios written in gherkin language in feature file
1. As its a open discussion in Testing world on what is the correct way of writting scenarios I have learnt from experts and believe writing scenarios in a declarative way is the correct approach.
1. The term BDD itself defines Behaviour driven development, I prefer speaking of behavior of application instead of Actions of the application. so modified the test scenarios accordingly.I choose the optimal way to make scennario understandable to every user who reads the scenarios

 
#### Moving rest calls and processing of response data to test API methods class
 
As part of refractoring the code, I created a seperate class TestAPIMethods. As its evident that in real world there will be several api Calls and  a lot of data needs to be processed for each endpoint. So, I choose the approach of Only assertions in Step definition file and all the requests and data processing methods into a seperate class. I followed page object pattern, which is suggested way for maintainability This is typical page objecct pattern which is a very famous approach for testing frameworks. 

# CI/CD

I have good experience with Jenkins and circleCI, this is the first time I'm dealing with GitlabCI but I have configured is correctly to best of my kowledge.

# Reporting

HTML reporting is also set as artifacts and uploaded to public repository for everyone to access the last run reports are available in the link below

https://sivagarjala.gitlab.io/-/leasePlanAssignment/-/jobs/1908733599/artifacts/public/serenity/index.html

Also another way of navigating to reports is *"Browse"* the artifacts from deploy stage of the job to path /target/site/serenit/index.html 


**Note**: _**There are 8 scenarios in this project 4 of them were failing as I assume every product to have the product title with product name in the title**_
