package api;

import io.restassured.response.Response;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;
import org.json.JSONArray;
import org.json.JSONObject;

import static org.assertj.core.api.Assertions.assertThat;


public class TestApiMethods extends PageObject{

    @Step
    public void getProductDetails(String productName){
        Response response =SerenityRest.given().log().all().get(SerenityRest.getDefaultBasePath()+"/search/test/"+productName);
        Serenity.setSessionVariable("GetProductDetailsResponseStatusCode").to(response.getStatusCode());
        Serenity.setSessionVariable("GetProductDetailsResponse").to(response.getBody().asString());

    }

    @Step
    public boolean verifyTitleContent(String productName){
        JSONArray productDetailsArray =new JSONArray(Serenity.sessionVariableCalled("GetProductDetailsResponse").toString());
        productDetailsArray.forEach((e)->{
            JSONObject productDetailsObject= new JSONObject(e.toString());
            assertThat(productDetailsObject.get("title").toString().toLowerCase().contains(productName)).as("error in product"+productDetailsObject.get("title").toString()).isTrue();
        });

        return true;
    }

    @Step
    public boolean isErrorDisplayed(){
        JSONObject responseObject =new JSONObject(Serenity.sessionVariableCalled("GetProductDetailsResponse").toString());
        JSONObject errorObject = new JSONObject(responseObject.get("detail").toString());
        return errorObject.get("error").equals(true);
    }

}

