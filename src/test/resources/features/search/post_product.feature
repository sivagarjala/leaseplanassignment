Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/test/{product} for getting the products.
### Available products: "apple", "mango", "tofu", "water"
### Prepare Positive and negative scenarios
Background:
  Given The heroku server application is running
  @Smoke @Regression
  Scenario Outline: Verify  valid response is received when requested for a valid input
    When user request for a input with <Product> as valid product
    Then user receives a valid response
    Examples:
      | Product |
      | apple   |
      | mango   |

  @Regression
  Scenario Outline: Verify  title content for each product retrieved
    When user request for a input with <Product> as valid product
    Then title for each <Product> is displayed as expected
    Examples:
      | Product |
      | tofu    |
      | water   |
      | apple   |
      | mango   |
  @Smoke @Regression
  Scenario Outline: Verify error response is received when requested for a invalid product
    When user request for a input with <Product> as invalid product
    Then user receives a error response
    And error message is displayed
    Examples:
      | Product |
      | car     |
      | MANGO   |

